
# coding: utf-8

# In[2]:

import selenium
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.ui import Select
import time
import re
import string


# In[59]:

def scrape_func(date_from, date_to):

    url = "http://www.cpcb.gov.in/CAAQM/frmUserAvgReportCriteria.aspx"
    time_to_wait = 2
    driver.get(url)
    
    state = 'Karnataka'
    city = 'Bengaluru'
    station = 'BTM'
    param = 'Nitrogen Dioxide(NO2)'
    granularity = '15 Minute'

    select_form(time_to_wait, 'ddlState', state)
    
    select_form(time_to_wait, 'ddlCity', city)
    
    select_form(time_to_wait, 'ddlStation', station)
    
    select_form(time_to_wait, 'lstBoxChannelLeft', param)
    
    click(time_to_wait, 'btnAdd')

    select_form(time_to_wait, 'ddlCriteria', granularity)

    enter_text(time_to_wait, 'txtDateFrom', date_from)
    
    enter_text(time_to_wait, 'txtDateTo', date_to)

    click(time_to_wait, 'btnSubmit')
    
    download_click (time_to_wait)
    
    
def download_click(time_to_wait):
    time.sleep(time_to_wait)
    try:
        download_btn = driver.find_element_by_id("btnExport")
        download_btn.click()
    except:
        download_click(time_to_wait)
        
def select_form(time_to_wait, elem_id, option_text):
    time.sleep(time_to_wait)
    try:
        select = Select(driver.find_element_by_id(elem_id))
        select.select_by_visible_text(option_text)
    except:
        select_form(time_to_wait, elem_id, option_text)
        
def click(time_to_wait, elem_id):
    time.sleep(time_to_wait)
    try:
        btn_click = driver.find_element_by_id(elem_id)
        btn_click.click()
    except:
        click(time_to_wait, elem_id)
        
def enter_text(time_to_wait, elem_id, text):
    time.sleep(time_to_wait)
    try:
        date = driver.find_element_by_id(elem_id)
        date.clear()
        date.send_keys(text) 
    except:
        enter_text(time_to_wait, elem_id, text)
        
        

#time.sleep(time_to_wait)
#select = Select(driver.find_element_by_id('ddlDateFrom'))
#select.select_by_visible_text('')
# Find the submit button using class name and click on it.
# submit_button = driver.find_element_by_class_name("loginbtn").click()


# In[63]:

driver = webdriver.Chrome("/Users/franklemuchahary/Documents/Data Science/Scrapping/chromedriver")


# In[64]:

date_from_list = ["01/01/2016", "01/02/2016", "01/03/2016", "01/04/2016", "01/05/2016", "01/06/2016", 
                 "01/07/2016", "01/08/2016", "01/09/2016", "01/10/2016", "01/11/2016", "01/12/2016"]
date_to_list = ["31/01/2016", "28/02/2016", "31/03/2016", "30/04/2016", "31/05/2016", "30/06/2016", 
                 "31/07/2016", "31/08/2016", "30/09/2016", "31/10/2016", "30/11/2016", "31/12/2016"]

for i in range(5, len(date_from_list)):
    scrape_func(date_from_list[i], date_to_list[i])
    print(date_from_list[i])
    print(date_to_list[i])
    print("Done")
    print("\n")

'''
date_from = "01/12/2016"
date_to = "30/12/2016"
date_to1 = "31/12/2016"
feb_date_to = "28/02/2016"

for i in range(12,13):
    if(i<10):
        date_from = date_from[0:3]+str(0)+str(i)+date_from[5:]
        if(i%2!=0 and i!=8 and i!=2):
            date_to = date_to[0:3]+str(0)+str(i)+date_to[5:]
            date_to1 = date_to1[0:3]+str(0)+str(i)+date_to1[5:]
            
            scrape_func(date_from, date_to)
        elif(i==2):
            date_to1 = date_to1[0:3]+str(0)+str(i)+date_to1[5:]
            date_to = date_to[0:3]+str(0)+str(i)+date_to[5:]
            
            scrape_func(date_from, feb_date_to)
        elif(i==8):
            date_to1 = date_to1[0:3]+str(0)+str(i)+date_to1[5:]
            date_to = date_to[0:3]+str(0)+str(i)+date_to[5:]
            
            scrape_func(date_from, date_to1)
        else:
            date_to1 = date_to1[0:3]+str(0)+str(i)+date_to1[5:]
            date_to = date_to[0:3]+str(0)+str(i)+date_to[5:]
            
            scrape_func(date_from, date_to1)
            
        print(date_from[0:3]+str(0)+str(i)+date_from[5:])
        print(date_to[0:3]+str(0)+str(i)+date_to[5:])
        
        print("Done")
    else:
        date_from = date_from[0:3]+str(i)+date_from[5:]
        if(i%2!=0 and i!=8 and i!=2):
            date_to = date_to[0:3]+str(i)+date_to[5:]
            date_to1 = date_to1[0:3]+str(i)+date_to1[5:]
            
            scrape_func(date_from, date_to)
        elif(i==2):
            date_to1 = date_to1[0:3]+str(i)+date_to1[5:]
            date_to = date_to[0:3]+str(i)+date_to[5:]
            
            scrape_func(date_from, feb_date_to)
        elif(i==8):
            date_to1 = date_to1[0:3]+str(i)+date_to1[5:]
            date_to = date_to[0:3]+str(i)+date_to[5:]
            
            scrape_func(date_from, date_to1)
        else:
            date_to1 = date_to1[0:3]+str(i)+date_to1[5:]
            date_to = date_to[0:3]+str(i)+date_to[5:]
            
            scrape_func(date_from, date_to1)   
        
        print(date_from[0:3]+str(i)+date_from[5:])
        print(date_to[0:3]+str(i)+date_to[5:])
        
        print("Done")
    print("\n")
'''